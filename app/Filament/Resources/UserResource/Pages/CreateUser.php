<?php

namespace App\Filament\Resources\UserResource\Pages;

use App\Events\UserCreated;
use App\Filament\Resources\UserResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateUser extends CreateRecord
{
    protected static string $resource = UserResource::class;

    public function afterCreate()
    {
        UserCreated::broadcast($this->record);
    }

}
